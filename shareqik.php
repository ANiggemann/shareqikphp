<?php
// shareqik.php
// Andreas Niggemann, Speyer
//
// Versions:
//            01.10.2019 Separate upload limits for host and guests (OTA)
//            29.09.2019 Directory display
//            28.09.2019 Multi language support
//            27.09.2019 Special functions, option for file size limit, display log function
//            25.09.2019 Options for one-time access 
//            23.09.2019 Configuration in a separate file
//            21.09.2019 Comment input, sessions
//            18.09.2019 Logging
//            17.09.2019 New HTML, integrated error messages, GET-Download
//            15.09.2019 String for allowed extensions instead of array
//            14.09.2019 New HTML, displays URL and autoselect URL for clipboard
//            06.09.2019 Option for removing old uploaded files
//            03.09.2019 Valid HTML
//            31.08.2019 Adapt HTML for mobile devices
//            18.08.2019 Base
// 
// QR Generator: http://phpqrcode.sourceforge.net/

include('phpqrcode.php');

$this_script = ltrim(htmlspecialchars($_SERVER['PHP_SELF']),'/');
include('config_' . $this_script);

if ($session_timeout > 0) {
    $lifetime = $session_timeout * 60;
    if (session_status() == PHP_SESSION_ACTIVE) {
        if (isset($_COOKIE[session_name()])) {
            unset($_COOKIE[session_name()]);
            setcookie(session_name(), '', time() - 3600, '/'); // empty value and old timestamp
        }
        session_unset();
        session_destroy();
    }
    session_start();
    setcookie(session_name(),session_id(),time()+$lifetime);
    if (isset($_GET['lang']) && file_exists($_GET['lang'] . '_' . $this_script))
      $_SESSION['LANG'] = $_GET['lang'];
    if (!isset($_SESSION['LANG']))
      $_SESSION['LANG'] = $ui_language;
    $ui_language = $_SESSION['LANG'];
}
else {
    $lifetime = 0;
    $admin_key = ""; // No admin functions without sessions
    if (session_status() == PHP_SESSION_ACTIVE) {
        session_unset();
        session_destroy();
    }
}

include($ui_language . '_' . $this_script);

$link                = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://" . $_SERVER['HTTP_HOST'] . '/' . $this_script; 
$log_file            = $upload_directory . "../" . pathinfo($this_script, PATHINFO_FILENAME) . ".log";
$message             = $lang['choosefileorcamera'];
$comment_text        = $lang[strtolower(str_replace(' ', '', $comment_text))];
$ota_id_length       = 30;
$ota_filename_prefix = "ota_";
$special_request     = false;

function process_logout()
{
    log_entry(__FUNCTION__, "", 9);
    if (session_status() == PHP_SESSION_ACTIVE) {
        session_unset();
        session_destroy();
    }
}

function log_entry($msg1, $msg2, $for_log_level)
{
    global $upload_directory, $log_level, $log_ip, $log_file;
    if ($for_log_level <= $log_level) {
        $user_ip = ($log_ip ? $_SERVER['REMOTE_ADDR'] : "");
        $dataToLog = array( date("Y-m-d H:i:s"), $user_ip, $msg1, $msg2 );
        $data = implode(" - ", $dataToLog) . PHP_EOL;
        file_put_contents($log_file, $data, FILE_APPEND);
    }
}

function show_dir($upl_dir, $adminkey)
{
    global $lang;
    if (($_SESSION['KEY'] == hash("sha512", $adminkey)) && ($upl_dir != "")) {
        log_entry(__FUNCTION__, "", 9);
        generate_html_header(0);
        echo '    <body class="dirdisplay">' . "\r\n";
        $counter = 0;
        foreach (glob($upl_dir . "*.*") as $fname)
        {
            echo '        ' . date("Y-m-d H:i:s", filectime($fname)) . ' - ' . pathinfo($fname, PATHINFO_BASENAME) . '<br>' . "\r\n";
            $counter++;
        }
        echo '        ' . $counter . ' ' . $lang['numberoffiles'] . "\r\n"; 
        echo '    </body>' . "\r\n"; 
        echo '</html>' . "\r\n";
        return true;
    }
}

function show_log($log, $adminkey)
{
    if ($_SESSION['KEY'] == hash("sha512", $adminkey)) {
        log_entry(__FUNCTION__, "", 9);
        generate_html_header(0);
        echo '    <body class="logdisplay">' . "\r\n";
        echo nl2br(file_get_contents($log)). "\r\n";
        echo '    </body>' . "\r\n"; 
        echo '</html>' . "\r\n";
        return true;
    }
}

function get_random_name($number_of_chars) // 0 = returns random_cset
{
    $random_cset = '123456789ABCDEFGHIJKLMNPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
    $retval = $random_cset;
    if ($number_of_chars > 0) {
        $retval = "";
        for ($i = 1; $i <= $number_of_chars; $i++)
            $retval .= substr($random_cset, rand(0, strlen($random_cset) - 1), 1);
        log_entry("generate random name", $retval, 10);
    }
    return $retval;
}

function get_languages()
{
    global $this_script;
    if (!isset($_SESSION['LANGUAGES'])) {
        $pattern = "??_" . $this_script;
        $retval = "";
        foreach (glob($pattern) as $fname)
            $retval .= "," . substr($fname, 0, 2);
        $_SESSION['LANGUAGES'] = $retval;
    }
    else 
        $retval = $_SESSION['LANGUAGES'];
    return substr($retval, 1);
}

function remove_old_files($dpath, $fn_prefix, $minutes) // Remove old files or ota ids
{
    global $lang;
    log_entry(__FUNCTION__, "", 10);
    if (($dpath != "") && ($fn_prefix != "")) { // Remove only with upload directory and file prefix
        $pattern = $fn_prefix . "*.*";
        $now = time();
        foreach (glob($dpath . $pattern) as $fname)
            if ($now - filectime($fname)  >= 60 * $minutes) {
                unlink($fname);
                log_entry($lang['deleted'], pathinfo($fname, PATHINFO_BASENAME), 2);
            }
    }
}

function cleanup_old_files($upload_directory, $filename_prefix, $keep_files_minutes, $ota_filename_prefix, $ota_timeout)
{
    if ($keep_files_minutes > 0) 
        remove_old_files($upload_directory, $filename_prefix, $keep_files_minutes);
    if ($ota_timeout > 0)
        remove_old_files($upload_directory, $ota_filename_prefix, $ota_timeout);
    remove_old_files($upload_directory, $ota_filename_prefix, $keep_files_minutes); // Remove ota anyway
}

function clean_upload_dir($dpath)
{
    global $lang;
    log_entry($lang['removeallfilesfromuploaddirectory'], $dpath, 2);
    if ($dpath != "") // Remove only with upload directory
        foreach (glob($dpath . "*.*") as $fname) {
            unlink($fname);
            log_entry($lang['deleted'], pathinfo($fname, PATHINFO_BASENAME), 2);
        }
}

function error_msg($error_number, $extra, $log_it)
{
    global $lang;
    switch ($error_number) {
        case 0: $msg = $lang['noerror']; break;
        case 1: $msg = $lang['illegalextension']; break;
        case 2: $msg = $lang['filenamemissing']; break;
        case 3: $msg = $lang['accessdenied']; break;
        case 4: $msg = $lang['onetimeaccessdenied']; break;
        case 5: $msg = $lang['filetoolarge']; break;
    }
    $log_msg = ($extra != "" ? $msg . ": " . $extra : $msg);
    if ($log_it)
        log_entry($log_msg, $lang['error'], 1);
    return "!" . $msg;
}

function exec_special_functions($function_name)
{
    global $upload_directory, $log_file, $link;
    log_entry("exec_special_functions", $function_name, 9);
    $retval = false;
    switch ($function_name) {
        case "dir" :
            generate_html_header(0);
            echo '<body><script>window.open("' . $link . '?dir=show", "_blank");</script></body></html>';
            break;
        case "log" :
            if (file_exists($log_file)) {
                generate_html_header(0);
                echo '<body><script>window.open("' . $link . '?log=show", "_blank");</script></body></html>';
            }
            break;
        case "removelog" : 
            if (file_exists($log_file))
                unlink($log_file); 
            break;
        case "clean" : clean_upload_dir($upload_directory); break;
    }
    return $retval;
}

function generate_getfile_html($cType, $fn, $full_fn, $cmt)
{
    log_entry(__FUNCTION__, "", 9);
    if (strpos($cType, "image/") === false) {// no image, file download
        header("Content-Type: " . $cType);
        header("Content-Disposition: attachment; filename=\"" . $fn . "\"");
        readfile($full_fn);
    }
    else { // show image
        $binary = fread(fopen($full_fn, "r"), filesize($full_fn));
        generate_html_header(0);
        echo '    <body class="bodyformat">' . "\r\n";
        echo '        <div class="commentdiv"><label>' . $cmt . '</label></div>' . "\r\n";
        echo '        <div><img src="data:' . $cType . ';base64,' . base64_encode($binary) . '" alt="' . $fn . '"/></div>' . "\r\n";
        echo '    </body>' . "\r\n";
        echo '</html>' . "\r\n";
    }
}

function process_get_file($upl_dir)
{
    global $lang;
    log_entry(__FUNCTION__, "", 9);
    $fname = $_GET['file'];
    $comment = $_GET['comment'];
    $full_fname = $upl_dir . $fname;
    if(file_exists ($full_fname)) {
        $ext = strtolower(pathinfo($fname, PATHINFO_EXTENSION));
        $contentType = (strpos("bmp, jpg, jpeg, jpe, png, gif, tif, tiff, fif, ief, ", $ext . ",") === false ? "application/" . $ext : "image/" . $ext); 
        $contentType = ($ext == "tif" ? "image/tiff" : $contentType);
        $contentType = ($ext == "jpe" ? "image/jpeg" : $contentType);
        $contentType = ($ext == "jpg" ? "image/jpeg" : $contentType);
        generate_getfile_html($contentType, $fname, $full_fname, $comment);
        log_entry($lang['downloaded'], $full_fname, 1);
    }
    else
        log_entry($lang['notavailable'], $full_fname, 1);
    return true;
}

function generate_ota($upl_dir, $ota_fn_prefix, $ota_length, $lnk)
{
    global $lang, $comment, $comment_text, $url, $qrfile_path;
    log_entry(__FUNCTION__, "", 9);
    $_SESSION['KEY_SET'] = 4;
    $comment = "";
    $comment_text = "";
    $random_name = get_random_name($ota_length);
    $url = $lnk . "?ota=" . $random_name; 
    $qrfile_path  = $upl_dir . $ota_fn_prefix . $random_name . '.png';
    QRcode::png($url, $qrfile_path); // Generate and store QR code from URL
    log_entry($lang['otacreated'], $random_name, 1);
}

function process_ota($ota_id_to_check, $upl_dir, $ota_length)
{
    global $lang, $message, $qrfile_path, $url, $lifetime, $comment_text, $keep_files_minutes, $key, $ota_filename_prefix;
    log_entry("process_ota", $ota_id_to_check, 9);
    $ota_id = preg_match("/[" . get_random_name(0) . "]{" . $ota_length . "}/", $ota_id_to_check, $ota_id_feld);
    $ota_id = $ota_id_feld[0];
    $message = error_msg(4, $ota_id_to_check, false);
    $_SESSION['KEY_SET'] = 99; // Preset error display only (if error)
    $key = "";
    $comment_text = "";
    if ($ota_id != "") {
        $ota_filename = $upl_dir . $ota_filename_prefix . $ota_id . '.png';
        if (file_exists($ota_filename)) {
            $message = "";
            $qrfile_path = "";
            $url = "";
            $lifetime = 0;
            $keep_files_minutes = 0;
            $_SESSION['KEY_SET'] = 2; // OTA mode
            $_SESSION['OTA_ID'] = $ota_id;
        }
        else {
            log_entry($lang['otaidmissing'], $ota_id_to_check, 1);
        }
    }
}

function remove_ota($upl_dir, $ota_fn_prefix)
{
    global $lang;
    if (isset($_SESSION['OTA_ID'])) {
        log_entry("check for OTA id", $_SESSION['OTA_ID'], 9);
        $fname = $upl_dir . $ota_fn_prefix . $_SESSION['OTA_ID'] . ".png";
        if (file_exists($fname)) {
            unlink($fname);
            log_entry($lang['deleted'], pathinfo($fname, PATHINFO_BASENAME), 2);
        }
        unset($_SESSION['OTA_ID']);
    }
}

function upload_file($upl_dir, $all_ext, $fn_prefix, $ota_tout, $ota_fn_prefix, $ltime, $max_size)
{
    global $lang, $url, $comment, $qrfile_path, $message, $link;
    log_entry(__FUNCTION__, "", 9);
    if ($ltime > 0) {
        $_SESSION['COMMENT'] = $comment;
        $_SESSION['KEY_SET'] = ($_SESSION['KEY_SET'] != 2 ? 1 : 3);
    }
    $source_name = (!empty($_FILES['cameraToUpload']['name'])) ? 'cameraToUpload' : 'fileToUpload';
    // Get filename and extension of uploaded file
    $filename  = pathinfo($_FILES[$source_name]['name'], PATHINFO_FILENAME);
    log_entry($filename, $source_name, 1);
    if (trim($filename != "")) { // Filename there
        $extension = strtolower(pathinfo($_FILES[$source_name]['name'], PATHINFO_EXTENSION));
        $filename_plus_ext = $filename. "." . $extension;
        $act_file_size = $_FILES[$source_name]['size'];
        if (($max_size <= 0) || ($act_file_size <= $max_size * 1024)) {
            if ($comment == "") 
                $comment = $filename_plus_ext;
            $allowed_ext = explode(",", $all_ext); 
            if (in_array($extension, $allowed_ext)) { // Check extension
                $new_filename = $fn_prefix . get_random_name(10) . "_" . $filename_plus_ext; // Build new filename
                $new_path     = $upl_dir . $new_filename;
                $qrfile_path  = $new_path . '.png';
                $url = $link . "?comment=" . urlencode($comment) . "&file=" . urlencode($new_filename);
                move_uploaded_file($_FILES[$source_name]['tmp_name'], $new_path); // Move uploaded file to directory uploads/files/
                QRcode::png($url, $qrfile_path); // Generate and store QR code from URL
                log_entry($filename_plus_ext, $lang['uploadedto'] . " " . $new_filename, 1);
                if ($ota_tout == -1)
                    remove_ota($upl_dir, $ota_fn_prefix);
            }
            else
                $message = error_msg(1, $filename_plus_ext, true);
        }
        else
            $message = error_msg(5, $filename_plus_ext, true);
    }
    else
        $message = error_msg(2, "", true);
}

function generate_html_styles()
{
    log_entry(__FUNCTION__, "", 9);
    echo '        <style>' . "\r\n";
    echo '            input[type="file"] { display: none; }' . "\r\n";
    echo '            .baseelement { width: 100%; min-width: 150px; max-width: 400px; padding: 5px 0px; }' . "\r\n";
    echo '            .textform { color: black; text-align: center; text-decoration: none; display: inline-block; }' . "\r\n";
    echo '            .alarmform { background-color: red; color: white; }' . "\r\n";
    echo '            .button { background-color: #4CAF50; border: none; color: white; text-align: center; text-decoration: none; display: inline-block; font-size: 16px; border-radius: 8px; }' . "\r\n";
    echo '            .submitbutton { height: 50px; }' . "\r\n";
    echo '            .browsebutton { height: 20px; }' . "\r\n";
    echo '            .keydiv { height: 30px; line-height: 30px; }' . "\r\n";
    echo '            .separatordiv { font-size:10px; height: 12px; width: 100%; text-align: center; }' . "\r\n";
    echo '            .bodyformat { font-family: verdana; }' . "\r\n";
    echo '            .footer { font-size:10px; text-align: center; }' . "\r\n";
    echo '            .removeline { font-size:10px; text-align: center; }' . "\r\n";
    echo '            .dirdisplay { font-family: monospace; font-size: 12px; }' . "\r\n";
    echo '            .logdisplay { font-family: monospace; font-size: 12px; }' . "\r\n";
    echo '            .commentdiv { height: 30px; line-height: 30px; }' . "\r\n";
    echo '        </style>' . "\r\n";
}

function generate_html_header($ltime)
{
    global $ui_language;
    log_entry(__FUNCTION__, "", 9);
    echo '<!DOCTYPE html>' . "\r\n";
    echo '<html lang="' . $ui_language . '">' . "\r\n";
    echo '    <head>' . "\r\n";
    echo '        <meta charset="utf-8">' . "\r\n";
    echo '        <meta content="width=device-width, initial-scale=1.0" name="viewport"/>' . "\r\n";
    if (($_SESSION['KEY_SET'] == 1) && ($ltime > 0))
        echo '        <meta http-equiv="refresh" content="' . strval($ltime + 5) . '">' . "\r\n";    
    echo '        <title>ShareQik</title>' . "\r\n";
    generate_html_styles();
    echo '    </head>' . "\r\n";
}

function generate_html_qr_box($qr_file, $qr_url, $keep_minutes, $ltime)
{
    global $lang;
    log_entry(__FUNCTION__, "", 9);
    if (file_exists($qr_file)) {
        echo '            <div>' . "\r\n";
        echo '                <a href="' . $qr_url . ' " target="_blank">' . "\r\n";
        echo '                    <img class="baseelement" src="' . $qr_file . '" alt="' . $qr_url . '"/>' . "\r\n"; // Show QR Code
        echo '                </a>' . "\r\n";
        echo '            </div>' . "\r\n";
        if ($ltime > 0) 
            echo '            <div><p class="baseelement removeline">' . $lang['willberemovedafter'] . ' ' . $keep_minutes . ' ' . $lang['minutes'] . '</p></div>' . "\r\n";
        echo '            <div>' . "\r\n";
        echo '                <textarea  class="baseelement" name="url" rows="5" cols="30" autofocus="autofocus" onfocus="this.select()" readonly>' . $qr_url . '</textarea>' . "\r\n"; //Show URL
        echo '            </div>' . "\r\n";
    }
}

function generate_main_html($qr_file, $qr_url, $msg, $with_key, $ltime, $comment_input, $script, $keep_minutes, $max_upload_size)
{
    global $lang;
    log_entry(__FUNCTION__, "", 9);
    
    generate_html_header($ltime);
    
    echo '    <body class="bodyformat">' . "\r\n";
    echo '        <form action="' . $script . '" method="post" enctype="multipart/form-data">' . "\r\n";
    if (($_SESSION['KEY_SET'] != 3) && ($_SESSION['KEY_SET'] != 4)) {
        if ($with_key && !isset($_SESSION['KEY_SET'])) {
            echo '            <div class="keydiv">' . "\r\n";
            echo '                <input class= "baseelement" type="password" id="key" name="key" placeholder="' . $lang['enteraccesskey'] . '" autofocus="autofocus">' . "\r\n";
            echo '            </div>' . "\r\n";
            echo '            <div class="separatordiv"></div>' . "\r\n";
        }
        if ($comment_input != "") {
            echo '            <div class="keydiv">' . "\r\n";
            echo '                <input class= "baseelement" type="text" id="comment" name="comment" value="' . $_SESSION['COMMENT'] . '" placeholder="' . $comment_input . '">' . "\r\n";
            echo '            </div>' . "\r\n";
        }
        if ($msg != "") {
            $msgClass = "textform";
            if ($msg[0] == '!') {
                $msgClass .= " alarmform";
                $msg =  substr($msg, 1); // Remove alarm sign
            }
            echo '        <div><label class="baseelement ' . $msgClass . '">' . $msg . '</label></div>' . "\r\n";
        }
        if ($_SESSION['KEY_SET'] != 99) { //no ota error
            echo '            <div>' . "\r\n";
            echo '                <label for="fileToUpload" class="baseelement button browsebutton">' . $lang['browse'] . '</label>' . "\r\n";
            echo '                <input name="fileToUpload" id="fileToUpload" type="file"/>' . "\r\n";
            echo '            </div>' . "\r\n";
            echo '            <div class="baseelement separatordiv">' . "\r\n";
            echo '            <div>' . "\r\n";
            echo '                <label for="cameraToUpload" class="baseelement button browsebutton">' . $lang['camera'] . '</label>' . "\r\n";
            echo '                <input name="cameraToUpload" value="cameraToUpload" id="cameraToUpload" type="file" accept="image/*" capture="camera"/>' . "\r\n";
            echo '            </div>' . "\r\n";
            echo '            <div class="baseelement separatordiv">' . "\r\n";
            if ($max_upload_size > 0)
                echo '                (' . $lang['maxuploadsize'] . ': ' . $max_upload_size . $lang['kb'] . ')' . "\r\n";
            echo '            </div>' . "\r\n";
            echo '            <div>' . "\r\n";
            echo '                <input class="baseelement button submitbutton" type="submit" value="' . $lang['upload'] . '" name="submit">' . "\r\n";
            echo '            </div>' . "\r\n";
        }
    }

    generate_html_qr_box($qr_file, $qr_url, $keep_minutes, $ltime);

    echo '        </form>' . "\r\n";
    if (isset($_SESSION) && ($_SESSION['KEY_SET'] != "")) {
        echo '        <p class="baseelement footer">' . "\r\n";
        echo '            <a href="' . $link . '?logout=1">Logout</a>' . "\r\n";
        echo '        </p>' . "\r\n";
    }
    if ($ltime > 0) {
        echo '        <p class="baseelement footer">' . "\r\n";
        echo '            <label>' . $lang['languages'] . ': </label>' . "\r\n";
        foreach (explode(",", get_languages()) as $language)
            echo '            <a href="' . $link . '?lang=' . $language . '">' . $language . '</a>' . "\r\n";
        echo '        </p>' . "\r\n";
    }
    echo '        <p class="baseelement footer"><a href="https://gitlab.com/ANiggemann/shareqikphp" target="_blank">ShareQik (c) Andreas Niggemann</a></p>' . "\r\n";
    echo '    </body>' . "\r\n";
    echo '</html>' . "\r\n";
    if ($_SESSION['KEY_SET'] > 2)
        process_logout();
}



/*
        MAIN program
*/

cleanup_old_files($upload_directory, $filename_prefix, $keep_files_minutes, $ota_filename_prefix, $ota_timeout);

$max_size_of_file = $max_file_size;

// Process upload request
if (isset($_REQUEST["submit"])) {
    $input_key = $_POST['key'];
    $_SESSION['KEY'] = hash("sha512", $input_key);
    $comment = $_POST['comment'];
    if (($input_key == $admin_key) && ($admin_key != "")) {
        $special = strtolower($comment);
        if (($special != "") && (in_array($special, explode(",", $special_functions)))) 
            $special_request = exec_special_functions($special);
        else
            generate_ota($upload_directory, $ota_filename_prefix, $ota_id_length, $link);
    }
    else if (($input_key == $key) || isset($_SESSION['KEY_SET'])) // Upload file and generate QR Code
            upload_file($upload_directory, $allowed_extensions, $filename_prefix, $ota_timeout, $ota_filename_prefix, $lifetime, $max_size_of_file);
        else 
            $message = error_msg(3, $input_key, true);
}

// Process GET requests 
if ($_GET['logout'] == "1")
    process_logout();
else if ($_GET['dir'] == "show")
    $special_request = show_dir($upload_directory, $admin_key);
else if ($_GET['log'] == "show")
    $special_request = show_log($log_file, $admin_key);
else if (!empty($_GET['file']) && !preg_match('=/=', $_GET['file'])) 
    $special_request = process_get_file($upload_directory);
else if (!empty($_GET['ota'])) {
    $max_size_of_file = $ota_max_file_size;
    process_ota($_GET['ota'], $upload_directory, $ota_id_length);
}

if (!$special_request)
    generate_main_html($qrfile_path, $url, $message, $key != "", $lifetime, $comment_text, $this_script, $keep_files_minutes, $max_size_of_file);

?>
