<?php

// ----------------------------------
// Configuration:
$key                = ""; // Access key for authorization, change this
$upload_directory   = "uploads/files/"; // Server upload directory, must exist
$allowed_extensions = "png,jpg,jpeg,gif"; // File extensions that are allowed
$filename_prefix    = "shareqik_";
$keep_files_minutes = 0; // Keep uploaded files for max. minutes, 0 = dont delete uploaded files
$log_level          = 1; // 0 = no logging, 1 = log up/down/errors, 2 = log up/down/errors/deletes
$session_timeout    = 15; // Session timeout in minutes, 0 = no session & no cookie
$comment_text       = ""; // Placeholder for comment input, no comment input if empty
$admin_key          = ""; // admin key for giving one-time access or other special functions
$ota_timeout        = -1; // time in minutes for one-time access, -1 only for one transfer
$log_ip             = true; // Log user ip, false = dont log user ip
$special_functions  = "dir,log,removelog,clean"; // List of allowed special functions entered in the comment input
$max_file_size      = 0; // Maximum file size for uploads in kilobytes, 0 = no limit
$ota_max_file_size  = 25000; // Maximum file size for OTA uploads in kilobytes, 0 = no limit
$ui_language        = "en"; // Default user interface language
// ----------------------------------

?>
