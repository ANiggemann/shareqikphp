<?php

// Deutsch
$lang = array(

    'choosefileorcamera' => 'Datei oder Kamera wählen',
    'deleted' => 'gelöscht',
    'removeallfilesfromuploaddirectory' => 'Lösche alle Dateien aus dem Upload-Verzeichnis',
    'noerror' => 'Kein Fehler',
    'illegalextension' => 'Ungültiger Dateityp',
    'filenamemissing' => 'Dateiname fehlt',
    'accessdenied' => 'Zugang verboten',
    'onetimeaccessdenied' => 'Einmal-Zugang verboten',
    'filetoolarge' => 'Datei zu groß',
    'error' => 'Fehler',
    'downloaded' => 'heruntergeladen',
    'notavailable' => 'nicht verfügbar',
    'otacreated' => 'Einmal-Zugang erzeugt',
    'otaidmissing' => 'Einmal-Zugangs-Id fehlt',
    'uploadedto' => 'hochgeladen zu',
    'willberemovedafter' => 'Wird entfernt nach',
    'minutes' => 'Minuten',
    'enteraccesskey' => 'Zugangsschlüssel eingeben',
    'browse' => 'Durchsuchen...',
    'camera' => 'Kamera...',
    'maxuploadsize' => 'Max. Dateigröße',
    'kb' => 'KB',
    'upload' => 'Hochladen',
    'entercomment' => 'Kommentar eingeben',
    'languages' => 'Sprachen',
    'numberoffiles' => 'Dateien'

);

?>
