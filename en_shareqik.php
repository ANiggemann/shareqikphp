<?php

// English
$lang = array(

    'choosefileorcamera' => 'Choose file or camera',
    'deleted' => 'deleted',
    'removeallfilesfromuploaddirectory' => 'Remove all files from upload directory',
    'noerror' => 'No error',
    'illegalextension' => 'Illegal extension',
    'filenamemissing' => 'Filename missing',
    'accessdenied' => 'Access denied',
    'onetimeaccessdenied' => 'One-Time access denied',
    'filetoolarge' => 'File too large',
    'error' => 'ERROR',
    'downloaded' => 'downloaded',
    'notavailable' => 'not available',
    'otacreated' => 'ota created',
    'otaidmissing' => 'ota id missing',
    'uploadedto' => 'uploaded to',
    'willberemovedafter' => 'Will be removed after',
    'minutes' => 'minutes',
    'enteraccesskey' => 'Enter access key',
    'browse' => 'Browse...',
    'camera' => 'Camera...',
    'maxuploadsize' => 'Max. upload size',
    'kb' => 'KB',
    'upload' => 'Upload',
    'entercomment' => 'Enter comment',
    'languages' => 'Languages',
    'numberoffiles' => 'files'

);

?>