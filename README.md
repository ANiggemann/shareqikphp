# ShareQik PHP

<img align="right" src="ShareQikPHP.jpg">
ShareQik is a PHP script to upload files to a webspace and make these files available for download via QR Codes.<br>
Works with mobile devices (smartphones, tablets etc.) too.<br>
ShareQik is especially handy for photographers to transfer images in virtually no time to client devices.<br>
In addition, it can be used to transfer images from one mobile device to another with no direct connection.<br>
<br>
<br>
<br>
<br>
<br>

ShareQik Features
-
* Uploads a file to a webspace and generates a QR code to access the uploaded file
* Each uploaded file gets an unique filename that contains the original filename
* Direct upload from the camera on iOS and Android supported
* Limits allowed filetypes to png, jpg, jpeg and gif
* More allowed filetypes can be configured
* Shows the URL and autoselect it for clipboard
* Option for removing of old uploaded files
* Cron integration for removing of old uploaded files
* Option for logging
* Basic safeguarding via access key
* Session & Cookie optional with session timeout for multiple uploads
* Optional comment on download display
* Comment stays as default during session
* Optional different file size limits for host uploads and guest uploads (OTA)
* Optional one-time access for guests
* Special functions to show upload directory, show log file, remove log file and to empty the upload directory
* Multi language support (english and german language files included)
* Web App, no need for special apps on any device
* No need for any account on the receivers end
* Needs internet connection

Installation
-
Obtain the file phpqrcode.php from http://phpqrcode.sourceforge.net/<br>
The files shareqik.php, de_shareqik.php , en_shareqik.php, config_shareqik.php and phpqrcode.php can be stored (via SFTP etc.) in an arbitrary directory on a PHP enabled webspace.<br>
Define the subdirectories uploads/files and place htaccess into uploads to prevent tampering via uploaded PHP files. Rename htaccess to .htaccess (dot htaccess).<br>
ShareQik can be configured by modifying config_shareqik.php:<br>
// ----------------------------------<br>
// Configuration:<br>
$key                = ""; // Access key for authorization, change this<br>
$upload_directory   = "uploads/files/"; // Server upload directory, must exist<br>
$allowed_extensions = "png,jpg,jpeg,gif"; // File extensions that are allowed<br>
$filename_prefix    = "shareqik_";<br>
$keep_files_minutes = 0; // Keep uploaded files for max. minutes, 0 = dont delete uploaded files<br>
$log_level          = 1; // 0 = no logging, 1 = log up/down/errors, 2 = log up/down/errors/deletes<br>
$session_timeout    = 15; // Session timeout in minutes, 0 = no session & no cookie<br>
$comment_text       = "" // placeholder for comment input, no comment input if empty<br>
$admin_key          = ""; // admin key for giving one-time access or other special functions
$ota_timeout        = -1; // time in minutes for one-time access, -1 only for one transfer<br>
$log_ip             = true; // log user ip, false = dont log user ip<br>
$special_functions  = "dir,log,removelog,clean"; // List of allowed special functions entered in the comment input<br>
$max_file_size      = 0; // Maximum file size for uploads in kilobytes, 0 = no limit<br>
$ota_max_file_size  = 25000; // Maximum file size for OTA uploads in kilobytes, 0 = no limit<br>
$ui_language        = "en"; // Default user interface language<br>
// ----------------------------------<br>

To remove old uploaded files best practice is to create a cron job starting shareqik.php in a shorter interval than the configured $keep_files_minutes (see above). 
For instance, if $keep_files_minutes is set to 15 minutes, set the cron job to 5 minutes to ensure that no file will stay for more than 20 minutes. 
The cron job and normal usage of shareqik.php are not synchronized.

If logging is enabled, the file shareqik.log can be found at the top level of the upload directory.

The languages can be customized in the files en_shareqik.php for english or de_shareqik.php for german. Other languages can be added by uploading a language PHP file (i.e. fr_shareqik.php, es_shareqik.php etc.).
Switching from one language to the other is possible via URL parameters. I.e. shareqik.php?lang=de or shareqik.php?lang=en

Usage
-
Open the URL (example: https://testserver.xyz/sqdir/shareqik.php) of ShareQik. Press the Browse button and select an image file or choose the Camera button and take an image. 
Enter the access key and press Upload.
The file will be uploaded and a QR code appears. This QR code can be scanned by mobile devices to download the image file.  
The URL will be displayed as well and is autoselected for clipboard usage.

Special functions
-
Special functions can be entered in the comment input field<br>
* dir - Show content of upload directory
* log - Show log file
* removelog - Delete log file
* clean - Delete all files in upload directory

One-time access (OTA)
-
OTA is a possibility to grant guests that usually have no ShareQik access a temporary upload right. 
In this case, a ShareQik host generates a QR code that allows the guest to upload one file or multiple files as long as the OTA timeout is not expired.<br>
OTA procedure:<br>
* Host enters the special ota key into the key input field
* Host presses the Upload button without browsing for a file or camera
* ShareQik generates a QR code
* Guest scans this QR code, will be redirected to the upload page of ShareQik and uploads a file

Server on Android
-
By installing a web server (including PHP) on an Android phone it is possible to install and run ShareQik locally, without any internet connection. Consequently, direct file sharing between phones is possible.
* The web server should be downloaded and installed from Google Play (Example: KSWEB from Google Play https://play.google.com/store/apps/details?id=ru.kslabs.ksweb)
* The ShareQik files should be placed in the htdocs directory of this web server (see installation description above)
* The ShareQik application should be available locally via http:/localhost:8080/shareqik.php (port depends on the web server, 8080 usually)
* On Android it is possible to start a local WIFI hotspot so other phones can connect to the phone running ShareQik
* The other phones accesses the application via http://ip-address:8080/shareqik.php where ip-address is the ip address of the web server 

Hints
-
* ShareQikPHP has only basic safeguarding (via an access key) against unauthorized use. The upload directory should be secured via htaccess.
* On iOS and Android it is possible to generate a shortcut for shareqik.php on the home screen for easy access. 
  * iOS: https://www.igeeksblog.com/how-to-add-website-shorcuts-on-iphone-and-ipad-in-safari-ios-7/
  * Android: https://www.howtogeek.com/196087/how-to-add-websites-to-the-home-screen-on-any-smartphone-or-tablet/
* Old uploaded files are only removed if the keep_files_minutes option is set to greater than 0 and only if ShareQikPHP is called again.
